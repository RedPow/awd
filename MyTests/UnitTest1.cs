using Microsoft.VisualStudio.TestTools.UnitTesting;
using MyTests;
using System;
using System.IO;
using ���������;
namespace MyTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        // �� NUll ������
        public void TestOnNullString()
        {
            string str = null;
            Invert invert = new Invert(Convert.ToString(str));
            var result = "������ ������ null";
            var Exepted = invert.GetAnagram();
            Assert.AreEqual(Exepted,result);
        }
        [TestMethod]
        // �� ������ ����  ""
        public void TestOnEmptyString()
        {
            string str = null;
            Invert invert = new Invert(Convert.ToString(str));
            var result = "������ ������ null";
            var Exepted = invert.GetAnagram();
            Assert.AreEqual(Exepted, result);
        }
        [TestMethod]
        // �� ������ ���� "    "
        public void TestOnSpaceString1()
        {
            string str = "    ";
            Invert invert = new Invert(str);
            string Exepted = "    ";
            var result = invert.GetAnagram();
            Assert.AreEqual(Exepted, result);
        }
        [TestMethod]
        // �� ������ ���� " "
        public void TestOnSpaceString2()
        {
            string str = " ";
            Invert invert = new Invert(str);
            string Exepted = " ";
            var result = invert.GetAnagram();
            Assert.AreEqual(Exepted, result);
        }
        [TestMethod]
        // ���� �� ������������ ��������������
        public void TestOnInvert()
        {
            string Input = "123 !a!b!c! abcd";
            string Exepted = "123 !c!b!a! dcba";
            Invert invert = new Invert(Input);
            var result = invert.GetAnagram();
            Assert.AreEqual(Exepted, result);
        }
        [TestMethod]
        // ���� �� ������������ �������������� ����� � ������ �������� �������� �� ����.
        public void TestOnNumbersInvert()
        {
            string Input = "123 4543 %*%#$#";
            string Exepted = "123 4543 %*%#$#";
            Invert invert = new Invert(Input);
            var result = invert.GetAnagram();
            Assert.AreEqual(Exepted, result);
        }
    }
}
