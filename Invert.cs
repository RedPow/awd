﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Анаграмма
{
    public class Invert
    {
        //Наша строка  с N слов
        public char[] InputSentence;
        public string FixedStr;
        private int number_end_charter, number_begin_charter = -1;
        private char[] StringForReturn;
        //Конструктор
        public Invert(string str)
        {
            if (str == null)
            {
                FixedStr = str;
                return;
            }
            str += " ";
            FixedStr = str;
        }
        //меняет элементы 
        private void Swap(ref char first, ref char second)
        {
            char buf;
            buf = first;
            first = second;
            second = buf;
        }
        //Функция которая выдает true если есть хотябы одна буква в слове, если нет false
        public bool DoesWordHaveLetter(char[] word)
        {
            for (int i = 0; i < word.Length; i++)
                if (char.IsLetter(word[i]))
                    return true;
            return false;
        }
        //рекурсивная функция которая выводит букву с начала слова
        private char NextCharacterFromBegin(char[] symbol, int i)
        {
            if (char.IsLetter(symbol[i]))
            {
                number_begin_charter = i;
                return symbol[i];
            }
            else
                return NextCharacterFromBegin(symbol, i + 1);
        }
        // рекурсивная функция которая выводит букву с конца слова
        private char NextCharacterFromEnd(char[] symbol, int i)
        {
            if (0 > i)
                return ' ';
            if (char.IsLetter(symbol[i]))
            {
                number_end_charter = i;
                return symbol[i];
            }
            else
                return NextCharacterFromEnd(symbol, i - 1);
        }
        //сколько всех символов находится в чаровом массиве 
        private int HowSymbolHaveStr(char[] name)
        {
            
            int sum = 0;
            for (int i = 0; i < name.Length; i++)
                if (name[i] != '\0')
                    sum++;
            return sum;
        }
        //Меняет слово                                                                      
        private void Go_invert(char[] str)
        {         
            if (!DoesWordHaveLetter(str))
                return;
            char character_end;
            int str_Lenght = HowSymbolHaveStr(str);
            number_end_charter = str_Lenght+1;
            number_begin_charter = -1;
            char character_begin;
            int word = 0;
            for (int i = 0; i < str_Lenght; i++)
            {
                if (char.IsLetter(str[i]))
                    ++word;
            }
            for (int i = 0; i < str_Lenght / 2; i++)
            {

                character_end = NextCharacterFromEnd(str, (number_end_charter - 1));

                character_begin = NextCharacterFromBegin(str, (number_begin_charter + 1));

                Swap(ref str[number_begin_charter], ref str[number_end_charter]);
                if (word / 2 == i + 1)
                    break;
            }
        }
        //Делит слова и использет Go_invert(char[] str)                                     
        private void WordDivision(char[] sentence)
        {
            if(sentence[0] == ' '&& sentence.Length == 1)
            {
                    string Message = "строка пришла пустая";
                    StringForReturn = new char[Message.Length];
                    StringForReturn = Message.ToCharArray();
                    return;
            }
            int lenght_one_word = 0;
            for (int i = 0; i < sentence.Length; i++)
            {
                lenght_one_word++;
                if (sentence[i] == ' ' || i == sentence.Length - 1)
                {
                    int k = 0;
                    char[] one_word = new char[sentence.Length];
                    for (int j = i - (lenght_one_word - 1); j < i; j++)
                    {
                        one_word[k] = sentence[j];
                        k++;
                    }
                    if (one_word[0] != '\0')
                        Go_invert(one_word);
                    for (int j = 0; j < lenght_one_word - 1; j++)
                    {
                        sentence[i - (lenght_one_word - j - 1)] = one_word[j];
                        one_word[j] = ' ';
                    }
                    lenght_one_word = 0;
                }
            }
            StringForReturn = new char[sentence.Length - 1];
             for(int i = 0; i < sentence.Length- 1; i++)
            {
                StringForReturn[i] = sentence[i];
            }
        }
        //Основная функция которая задействует WordDivision();
        public string GetAnagram()
        {
            if (FixedStr == null)
                return "строка пришла null";
            InputSentence = FixedStr.ToCharArray();//Присваиваем входное значение нашей строке
            number_end_charter = InputSentence.Length;
            WordDivision(InputSentence);
            string str = new string(StringForReturn);
            return str;
        }
    }
}
